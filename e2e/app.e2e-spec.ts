import { GkfxProjectPage } from './app.po';

describe('gkfx-project App', () => {
  let page: GkfxProjectPage;

  beforeEach(() => {
    page = new GkfxProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
