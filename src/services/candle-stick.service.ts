import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import 'rxjs/Rx';
@Injectable()
export class CandleStickService {

    constructor(private http: Http) {}
    getCandleStickData() {
        return this.http.get('getHistoryUSFuturesEOD.json')
        .map((resp: Response) => {
            return resp.json();
        });
    }
}