import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import {HttpClient} from '@angular/common/http';

import 'rxjs/Rx';
import {Observable} from 'rxjs/Rx';

@Injectable()

export class DateBasedService {
    constructor(private http: Http,private httpClient: HttpClient) {}
    getDateBasedData() {
        return this.http.get('getHistoryUSFuturesTick.json')
        .map((resp: Response) => {
            return resp.json();
        });
    }
}