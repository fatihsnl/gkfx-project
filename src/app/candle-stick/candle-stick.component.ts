import { Component, OnInit } from '@angular/core';
import { CandleStickService } from "services/candle-stick.service";
import { CandleStick } from "model/candle-stick.model";

declare var AmCharts: any;
@Component({
  selector: 'app-candle-stick',
  templateUrl: './candle-stick.component.html',
  styleUrls: ['./candle-stick.component.css'],
  providers:[CandleStickService]
})
export class CandleStickComponent implements OnInit {
  chartObj: any;
  private candleStickArray: CandleStick[] = [];
  constructor(private candleStickService: CandleStickService) { }

  ngOnInit() {
    this.candleStickService.getCandleStickData().subscribe(
      data => {
        this.candleStickArray = data.results;
        const div = document.querySelector('#chart2Div');
        this.chartObj = AmCharts.makeChart( div, {
        "type": "serial",
        "theme": "light",
        "dataDateFormat":"YYYY-MM-DD",
        "valueAxes": [ {
          "position": "left"
        } ],
        "graphs": [ {
          "id": "g1",
          "proCandlesticks": true,
          "balloonText": "Open:<b>[[open]]</b><br>Low:<b>[[low]]</b><br>High:<b>[[high]]</b><br>Close:<b>[[close]]</b><br>",
          "closeField": "close",
          "fillColors": "#7f8da9",
          "highField": "high",
          "lineColor": "#7f8da9",
          "lineAlpha": 1,
          "lowField": "low",
          "fillAlphas": 0.9,
          "negativeFillColors": "#db4c3c",
          "negativeLineColor": "#db4c3c",
          "openField": "open",
          "title": "Price:",
          "type": "candlestick",
          "valueField": "close"
        } ],
        "chartScrollbar": {
          "graph": "g1",
          "graphType": "line",
          "scrollbarHeight": 30
        },
        "chartCursor": {
          "valueLineEnabled": true,
          "valueLineBalloonEnabled": true
        },
        "categoryField": "tradingDay",
        "categoryAxis": {
          "parseDates": true
        },
        "dataProvider": this.candleStickArray
        });
      }
    );
  }
}
