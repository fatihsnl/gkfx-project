import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CandleStickComponent } from "app/candle-stick/candle-stick.component";
import { DateBasedComponent } from "app/date-based/date-based.component";
import { HomeComponent } from "app/home/home.component";

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'date-based', component: DateBasedComponent},
  { path: 'candle-stick', component: CandleStickComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}