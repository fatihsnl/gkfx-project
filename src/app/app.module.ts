import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { DateBasedComponent } from './date-based/date-based.component';
import { CandleStickComponent } from './candle-stick/candle-stick.component';
import { DateBasedService } from "services/data-based.service";
import { CandleStickService } from "services/candle-stick.service";
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from "app/app-routing.module";
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    DateBasedComponent,
    CandleStickComponent,
    HomeComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [DateBasedService, CandleStickService],
  bootstrap: [AppComponent]
})
export class AppModule { }
