import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateBasedComponent } from './date-based.component';

describe('DateBasedComponent', () => {
  let component: DateBasedComponent;
  let fixture: ComponentFixture<DateBasedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateBasedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateBasedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
